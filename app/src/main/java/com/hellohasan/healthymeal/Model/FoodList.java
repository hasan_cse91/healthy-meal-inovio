
package com.hellohasan.healthymeal.Model;

import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FoodList implements Parcelable
{

    @SerializedName("total")
    @Expose
    private int total;
    @SerializedName("limit")
    @Expose
    private int limit;
    @SerializedName("skip")
    @Expose
    private int skip;
    @SerializedName("page")
    @Expose
    private int page;
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = new ArrayList<Datum>();
    public final static Parcelable.Creator<FoodList> CREATOR = new Creator<FoodList>() {


        @SuppressWarnings({
            "unchecked"
        })
        public FoodList createFromParcel(Parcel in) {
            FoodList instance = new FoodList();
            instance.total = ((int) in.readValue((int.class.getClassLoader())));
            instance.limit = ((int) in.readValue((int.class.getClassLoader())));
            instance.skip = ((int) in.readValue((int.class.getClassLoader())));
            instance.page = ((int) in.readValue((int.class.getClassLoader())));
            instance.success = ((boolean) in.readValue((boolean.class.getClassLoader())));
            instance.message = ((String) in.readValue((String.class.getClassLoader())));
            in.readList(instance.data, (com.hellohasan.healthymeal.Model.Datum.class.getClassLoader()));
            return instance;
        }

        public FoodList[] newArray(int size) {
            return (new FoodList[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The total
     */
    public int getTotal() {
        return total;
    }

    /**
     * 
     * @param total
     *     The total
     */
    public void setTotal(int total) {
        this.total = total;
    }

    /**
     * 
     * @return
     *     The limit
     */
    public int getLimit() {
        return limit;
    }

    /**
     * 
     * @param limit
     *     The limit
     */
    public void setLimit(int limit) {
        this.limit = limit;
    }

    /**
     * 
     * @return
     *     The skip
     */
    public int getSkip() {
        return skip;
    }

    /**
     * 
     * @param skip
     *     The skip
     */
    public void setSkip(int skip) {
        this.skip = skip;
    }

    /**
     * 
     * @return
     *     The page
     */
    public int getPage() {
        return page;
    }

    /**
     * 
     * @param page
     *     The page
     */
    public void setPage(int page) {
        this.page = page;
    }

    /**
     * 
     * @return
     *     The success
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * 
     * @param success
     *     The success
     */
    public void setSuccess(boolean success) {
        this.success = success;
    }

    /**
     * 
     * @return
     *     The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * 
     * @param message
     *     The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * 
     * @return
     *     The data
     */
    public List<Datum> getData() {
        return data;
    }

    /**
     * 
     * @param data
     *     The data
     */
    public void setData(List<Datum> data) {
        this.data = data;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(total);
        dest.writeValue(limit);
        dest.writeValue(skip);
        dest.writeValue(page);
        dest.writeValue(success);
        dest.writeValue(message);
        dest.writeList(data);
    }

    public int describeContents() {
        return  0;
    }

}
