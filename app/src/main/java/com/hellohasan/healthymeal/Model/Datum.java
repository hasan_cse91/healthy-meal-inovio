
package com.hellohasan.healthymeal.Model;

import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum implements Parcelable
{

    @SerializedName("_id")
    @Expose
    private String Id;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("price")
    @Expose
    private int price;
    @SerializedName("recipe")
    @Expose
    private String recipe;
    @SerializedName("ingredients")
    @Expose
    private String ingredients;
    @SerializedName("calorie")
    @Expose
    private String calorie;
    @SerializedName("protein")
    @Expose
    private String protein;
    @SerializedName("fat")
    @Expose
    private String fat;
    @SerializedName("carbs")
    @Expose
    private String carbs;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("youtubeUrl")
    @Expose
    private String youtubeUrl;
    @SerializedName("__v")
    @Expose
    private int V;
    @SerializedName("nutritionImage")
    @Expose
    private String nutritionImage;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("load")
    @Expose
    private int load;
    @SerializedName("images")
    @Expose
    private List<String> images = new ArrayList<String>();
    @SerializedName("provider")
    @Expose
    private List<Object> provider = new ArrayList<Object>();
    @SerializedName("tags")
    @Expose
    private List<Tag> tags = new ArrayList<Tag>();
    private boolean isAddedToCart;
    public final static Parcelable.Creator<Datum> CREATOR = new Creator<Datum>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Datum createFromParcel(Parcel in) {
            Datum instance = new Datum();
            instance.Id = ((String) in.readValue((String.class.getClassLoader())));
            instance.createdAt = ((String) in.readValue((String.class.getClassLoader())));
            instance.updatedAt = ((String) in.readValue((String.class.getClassLoader())));
            instance.name = ((String) in.readValue((String.class.getClassLoader())));
            instance.price = ((int) in.readValue((int.class.getClassLoader())));
            instance.recipe = ((String) in.readValue((String.class.getClassLoader())));
            instance.ingredients = ((String) in.readValue((String.class.getClassLoader())));
            instance.calorie = ((String) in.readValue((String.class.getClassLoader())));
            instance.protein = ((String) in.readValue((String.class.getClassLoader())));
            instance.fat = ((String) in.readValue((String.class.getClassLoader())));
            instance.carbs = ((String) in.readValue((String.class.getClassLoader())));
            instance.description = ((String) in.readValue((String.class.getClassLoader())));
            instance.youtubeUrl = ((String) in.readValue((String.class.getClassLoader())));
            instance.V = ((int) in.readValue((int.class.getClassLoader())));
            instance.nutritionImage = ((String) in.readValue((String.class.getClassLoader())));
            instance.status = ((String) in.readValue((String.class.getClassLoader())));
            instance.load = ((int) in.readValue((int.class.getClassLoader())));
            in.readList(instance.images, (java.lang.String.class.getClassLoader()));
            in.readList(instance.provider, (java.lang.Object.class.getClassLoader()));
            in.readList(instance.tags, (com.hellohasan.healthymeal.Model.Tag.class.getClassLoader()));
            instance.isAddedToCart = false;
            return instance;
        }

        public Datum[] newArray(int size) {
            return (new Datum[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The Id
     */
    public String getId() {
        return Id;
    }

    /**
     * 
     * @param Id
     *     The _id
     */
    public void setId(String Id) {
        this.Id = Id;
    }

    /**
     * 
     * @return
     *     The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * 
     * @param createdAt
     *     The createdAt
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 
     * @return
     *     The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * 
     * @param updatedAt
     *     The updatedAt
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The price
     */
    public int getPrice() {
        return price;
    }

    /**
     * 
     * @param price
     *     The price
     */
    public void setPrice(int price) {
        this.price = price;
    }

    /**
     * 
     * @return
     *     The recipe
     */
    public String getRecipe() {
        return recipe;
    }

    /**
     * 
     * @param recipe
     *     The recipe
     */
    public void setRecipe(String recipe) {
        this.recipe = recipe;
    }

    /**
     * 
     * @return
     *     The ingredients
     */
    public String getIngredients() {
        return ingredients;
    }

    /**
     * 
     * @param ingredients
     *     The ingredients
     */
    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    /**
     * 
     * @return
     *     The calorie
     */
    public String getCalorie() {
        return calorie;
    }

    /**
     * 
     * @param calorie
     *     The calorie
     */
    public void setCalorie(String calorie) {
        this.calorie = calorie;
    }

    /**
     * 
     * @return
     *     The protein
     */
    public String getProtein() {
        return protein;
    }

    /**
     * 
     * @param protein
     *     The protein
     */
    public void setProtein(String protein) {
        this.protein = protein;
    }

    /**
     * 
     * @return
     *     The fat
     */
    public String getFat() {
        return fat;
    }

    /**
     * 
     * @param fat
     *     The fat
     */
    public void setFat(String fat) {
        this.fat = fat;
    }

    /**
     * 
     * @return
     *     The carbs
     */
    public String getCarbs() {
        return carbs;
    }

    /**
     * 
     * @param carbs
     *     The carbs
     */
    public void setCarbs(String carbs) {
        this.carbs = carbs;
    }

    /**
     * 
     * @return
     *     The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * 
     * @param description
     *     The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 
     * @return
     *     The youtubeUrl
     */
    public String getYoutubeUrl() {
        return youtubeUrl;
    }

    /**
     * 
     * @param youtubeUrl
     *     The youtubeUrl
     */
    public void setYoutubeUrl(String youtubeUrl) {
        this.youtubeUrl = youtubeUrl;
    }

    /**
     * 
     * @return
     *     The V
     */
    public int getV() {
        return V;
    }

    /**
     * 
     * @param V
     *     The __v
     */
    public void setV(int V) {
        this.V = V;
    }

    /**
     * 
     * @return
     *     The nutritionImage
     */
    public String getNutritionImage() {
        return nutritionImage;
    }

    /**
     * 
     * @param nutritionImage
     *     The nutritionImage
     */
    public void setNutritionImage(String nutritionImage) {
        this.nutritionImage = nutritionImage;
    }

    /**
     * 
     * @return
     *     The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The load
     */
    public int getLoad() {
        return load;
    }

    /**
     * 
     * @param load
     *     The load
     */
    public void setLoad(int load) {
        this.load = load;
    }

    /**
     * 
     * @return
     *     The images
     */
    public List<String> getImages() {
        return images;
    }

    /**
     * 
     * @param images
     *     The images
     */
    public void setImages(List<String> images) {
        this.images = images;
    }

    /**
     * 
     * @return
     *     The provider
     */
    public List<Object> getProvider() {
        return provider;
    }

    /**
     * 
     * @param provider
     *     The provider
     */
    public void setProvider(List<Object> provider) {
        this.provider = provider;
    }

    /**
     * 
     * @return
     *     The tags
     */
    public List<Tag> getTags() {
        return tags;
    }

    /**
     * 
     * @param tags
     *     The tags
     */
    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public boolean isAddedToCart() {
        return isAddedToCart;
    }

    public void setAddedToCart(boolean addedToCart) {
        isAddedToCart = addedToCart;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(Id);
        dest.writeValue(createdAt);
        dest.writeValue(updatedAt);
        dest.writeValue(name);
        dest.writeValue(price);
        dest.writeValue(recipe);
        dest.writeValue(ingredients);
        dest.writeValue(calorie);
        dest.writeValue(protein);
        dest.writeValue(fat);
        dest.writeValue(carbs);
        dest.writeValue(description);
        dest.writeValue(youtubeUrl);
        dest.writeValue(V);
        dest.writeValue(nutritionImage);
        dest.writeValue(status);
        dest.writeValue(load);
        dest.writeList(images);
        dest.writeList(provider);
        dest.writeList(tags);
    }

    public int describeContents() {
        return  0;
    }

}
