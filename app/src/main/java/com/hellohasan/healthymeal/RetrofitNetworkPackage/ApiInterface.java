package com.hellohasan.healthymeal.RetrofitNetworkPackage;

import com.hellohasan.healthymeal.Model.FoodList;
import com.hellohasan.healthymeal.Model.TagList;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {

    @GET("api/foods")
    Call<FoodList> getFoodList();

    @GET("api/tags")
    Call<TagList> getTagList();

}

