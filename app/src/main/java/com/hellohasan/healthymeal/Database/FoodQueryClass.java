package com.hellohasan.healthymeal.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.google.gson.Gson;
import com.hellohasan.healthymeal.HelperClass.KeyNameClass;
import com.hellohasan.healthymeal.Model.Datum;
import com.hellohasan.healthymeal.Model.Tag;

import java.util.ArrayList;
import java.util.List;


public class FoodQueryClass {

    private SQLiteDatabase sqLiteDatabase;
    private DatabaseHandler databaseHandler;

    public FoodQueryClass(Context context){
        databaseHandler = new DatabaseHandler(context);
    }


    public void addToCartTable(Datum foodData){

        open();
        ContentValues contentValues = getContentValues(foodData);

        sqLiteDatabase.insert(KeyNameClass.CART_TABLE, null, contentValues);
        sqLiteDatabase.close();
        close();
    }


    public List<Datum> getAllFoodFromCart(){

        open();

        List<Datum> allFoodList = new ArrayList<>();
        String SELECT_QUERY = "SELECT * FROM " + KeyNameClass.CART_TABLE;

        Cursor cursor = sqLiteDatabase.rawQuery(SELECT_QUERY, null);
        String CREATE_CART_TABLE = "CREATE TABLE " + KeyNameClass.CART_TABLE + "("
                + KeyNameClass.FOOD_ID + " TEXT PRIMARY KEY, "
                + KeyNameClass.FOOD_NAME + " TEXT NOT NULL, "
                + KeyNameClass.IMAGE_URL + " TEXT NOT NULL, "
                + KeyNameClass.DESCRIPTION + " TEXT NOT NULL, "
                + KeyNameClass.PRICE + " TEXT NOT NULL, "
                + KeyNameClass.CALORIE + " TEXT NOT NULL, "
                + KeyNameClass.PROTEIN + " TEXT NOT NULL, "
                + KeyNameClass.FAT + " TEXT NOT NULL, "
                + KeyNameClass.TAGS + " TEXT NOT NULL "
                + ")";
        if(cursor!=null)
            if(cursor.moveToFirst()){
                do {
                    Datum foodData = new Datum();

                    foodData.setId(cursor.getString(0));
                    foodData.setName(cursor.getString(1));

                    List<String> imageList = new ArrayList<>();
                            imageList.add(cursor.getString(2));

                    foodData.setImages(imageList);
                    foodData.setDescription(cursor.getString(3));
                    foodData.setPrice(cursor.getInt(4));
                    foodData.setCalorie(cursor.getString(5));
                    foodData.setProtein(cursor.getString(6));
                    foodData.setFat(cursor.getString(7));

//                    String tagListString = cursor.getString(8);
//                    Gson gson = new Gson();
//                    List<Tag> tags = (List<Tag>) gson.fromJson(tagListString, Tag.class);
//
//                    foodData.setTags(tags);

                    allFoodList.add(foodData);

                }while (cursor.moveToNext());

                cursor.close();
            }
        close();

        return allFoodList;
    }

    public boolean isProductSelected(String productId){

        open();

        String SELECT_QUERY = "SELECT * FROM " + KeyNameClass.CART_TABLE + " WHERE " + KeyNameClass.FOOD_ID + " = '" + productId + "'";
        Cursor cursor = sqLiteDatabase.rawQuery(SELECT_QUERY, null);

        if(cursor.getCount()<=0){
            cursor.close();
            return false;
        }

        cursor.close();
        close();
        return true;
    }

    public void deleteProductById(String id){
        open();
        sqLiteDatabase.execSQL("DELETE FROM " + KeyNameClass.CART_TABLE + " WHERE "
                + KeyNameClass.FOOD_ID + " = '" + id + "'");
        close();
    }


    public void deleteAllProductFromCart(){
        open();
        sqLiteDatabase.execSQL("DELETE FROM " + KeyNameClass.CART_TABLE);
        close();
    }

    private ContentValues getContentValues(Datum foodData){

        ContentValues contentValues = new ContentValues();

        contentValues.put(KeyNameClass.FOOD_ID, foodData.getId());
        contentValues.put(KeyNameClass.FOOD_NAME, foodData.getName());
        contentValues.put(KeyNameClass.DESCRIPTION, foodData.getDescription());
        contentValues.put(KeyNameClass.IMAGE_URL, foodData.getImages().get(0));
        contentValues.put(KeyNameClass.PRICE, foodData.getPrice());

        Gson gson = new Gson();
        List<Tag> tagList = foodData.getTags();
        String tagListString = gson.toJson(tagList);

        contentValues.put(KeyNameClass.TAGS, tagListString);
        contentValues.put(KeyNameClass.CALORIE, foodData.getCalorie());
        contentValues.put(KeyNameClass.PROTEIN, foodData.getProtein());
        contentValues.put(KeyNameClass.FAT, foodData.getFat());

        return contentValues;
    }

    private void open() throws SQLException {
        sqLiteDatabase = databaseHandler.getWritableDatabase();
    }

    private void close() {
        databaseHandler.close();
    }


}