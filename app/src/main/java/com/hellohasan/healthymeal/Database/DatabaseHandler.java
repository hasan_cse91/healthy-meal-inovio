package com.hellohasan.healthymeal.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.hellohasan.healthymeal.HelperClass.KeyNameClass;


public class DatabaseHandler extends SQLiteOpenHelper {

    // All Static variables
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "healthymealdb";

    // Constructor
    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        // Create tables SQL execution
        String CREATE_CART_TABLE = "CREATE TABLE " + KeyNameClass.CART_TABLE + "("
                + KeyNameClass.FOOD_ID + " TEXT PRIMARY KEY, "
                + KeyNameClass.FOOD_NAME + " TEXT NOT NULL, "
                + KeyNameClass.IMAGE_URL + " TEXT NOT NULL, "
                + KeyNameClass.DESCRIPTION + " TEXT NOT NULL, "
                + KeyNameClass.PRICE + " TEXT NOT NULL, "
                + KeyNameClass.CALORIE + " TEXT NOT NULL, "
                + KeyNameClass.PROTEIN + " TEXT NOT NULL, "
                + KeyNameClass.FAT + " TEXT NOT NULL, "
                + KeyNameClass.TAGS + " TEXT NOT NULL "
                + ")";

        db.execSQL(CREATE_CART_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + KeyNameClass.CART_TABLE);

        // Create tables again
        onCreate(db);
    }

}
