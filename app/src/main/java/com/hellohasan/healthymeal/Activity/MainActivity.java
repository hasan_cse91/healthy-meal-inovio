package com.hellohasan.healthymeal.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.hellohasan.healthymeal.Fragment.FoodDetailsFragment;
import com.hellohasan.healthymeal.Fragment.FoodListFragment;
import com.hellohasan.healthymeal.Fragment.OrderFragment;
import com.hellohasan.healthymeal.HelperClass.ClickEventAction;
import com.hellohasan.healthymeal.HelperClass.Preferences;
import com.hellohasan.healthymeal.HelperClass.UtilityClass;
import com.hellohasan.healthymeal.Model.DataTag;
import com.hellohasan.healthymeal.Model.Datum;
import com.hellohasan.healthymeal.Model.Tag;
import com.hellohasan.healthymeal.Model.TagList;
import com.hellohasan.healthymeal.R;
import com.hellohasan.healthymeal.RetrofitNetworkPackage.ApiInterface;
import com.hellohasan.healthymeal.RetrofitNetworkPackage.RetrofitApiClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements ClickEventAction {

    private TextView cartTextView;
    private TextView tagTextView;
    private Preferences preferences;
    private ApiInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cartTextView = (TextView) findViewById(R.id.cartCounter);
        tagTextView = (TextView) findViewById(R.id.tags);
        preferences = Preferences.getInstance(this);
        apiInterface = RetrofitApiClient.getClient().create(ApiInterface.class);

        cartTextView.setText(String.valueOf(preferences.getNumberOfSelectedProduct()));

        getSupportFragmentManager().beginTransaction().replace(R.id.fragmentPortion, new FoodListFragment()).commit();

        if(UtilityClass.isNetworkAvailable(this)){

            Call<TagList> call = apiInterface.getTagList();
            call.enqueue(new Callback<TagList>() {
                @Override
                public void onResponse(Call<TagList> call, Response<TagList> response) {
                    TagList tagList = response.body();

                    if(tagList!=null && tagList.isSuccess()){
                        List<DataTag> tags = tagList.getDataTag();

                        for (DataTag tag: tags) {
                            tagTextView.append(tag.getName() + ", ");
                        }
                    }
                }

                @Override
                public void onFailure(Call<TagList> call, Throwable t) {

                }
            });
        }


    }

    public void buttonClick(View view) {
        view.startAnimation(UtilityClass.getAnimation());

        switch (view.getId()){

            case R.id.cartCounter:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragmentPortion, new OrderFragment()).commit();
                break;
        }
    }


    @Override
    public void updateNumberOfCart(int item) {
        cartTextView.setText(String.valueOf(item));
    }

    @Override
    public void showFoodDetails(Datum foodItem) {

        Gson gson = new Gson();
        String foodJsonString = gson.toJson(foodItem);

        FoodDetailsFragment fragment = new FoodDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putString("data", foodJsonString);
        fragment.setArguments(bundle);

        getSupportFragmentManager().beginTransaction().replace(R.id.fragmentPortion, fragment).commit();

    }

}