package com.hellohasan.healthymeal.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hellohasan.healthymeal.Database.FoodQueryClass;
import com.hellohasan.healthymeal.HelperClass.ClickEventAction;
import com.hellohasan.healthymeal.HelperClass.Preferences;
import com.hellohasan.healthymeal.Model.Datum;
import com.hellohasan.healthymeal.R;
import com.squareup.picasso.Picasso;

import java.util.List;


public class FoodListAdapter extends
        RecyclerView.Adapter<FoodListAdapter.ViewHolder>{

    private static List<Datum> foodDataList;
    // Store the context for easy access
    private Context mContext;
    private int lastPosition = -1;
    private View contactView;
    private static FoodQueryClass foodQueryClass;
    private static Preferences preferences;

    // Pass in the contact array into the constructor
    public FoodListAdapter(Context context, List<Datum> foods) {
        foodDataList = foods;
        mContext = context;
        foodQueryClass = new FoodQueryClass(mContext);
        preferences = Preferences.getInstance(mContext);
    }

    // Easy access to the context object in the recyclerview
    private Context getContext() {
        return mContext;
    }

    // Usually involves inflating a layout from XML and returning the holder
    @Override
    public FoodListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        contactView = inflater.inflate(R.layout.single_item_short_info, parent, false);

        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(contactView);
        return viewHolder;
    }

    // Involves populating data into the item through holder
    @Override
    public void onBindViewHolder(FoodListAdapter.ViewHolder holder, int position) {

        // Get the data model based on position
        Datum singleFood = foodDataList.get(position);
        String data;

        Picasso.with(getContext())
                .load(singleFood.getImages().get(0))
                .error(R.drawable.placeholder)
                .into(holder.featureGraphics);

        holder.foodNameTextView.setText(singleFood.getName());

        data = mContext.getString(R.string.calorie) + ": " + singleFood.getCalorie();
        holder.calorieTextView.setText(data);

        data = mContext.getString(R.string.price) + ": " + singleFood.getPrice() + " BDT";
        holder.priceTextView.setText(data);


        if(foodQueryClass.isProductSelected(singleFood.getId())) {
            singleFood.setAddedToCart(true);
            Picasso.with(getContext())
                    .load(R.drawable.ic_favorite_red)
                    .into(holder.addToCartImageView);
        }
        else {
            singleFood.setAddedToCart(false);
            Picasso.with(getContext())
                    .load(R.drawable.ic_favorite_border_red)
                    .into(holder.addToCartImageView);
        }

        setAnimation(contactView, position);
    }

    @Override
    public int getItemCount() {
        return foodDataList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        public ImageView featureGraphics;
        public TextView foodNameTextView;
        public TextView calorieTextView;
        public TextView priceTextView;
        public ImageView addToCartImageView;


        // We also create a constructor that accepts the entire item row
        // and does the view lookups to find each subview
        public ViewHolder(View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);

            featureGraphics = (ImageView) itemView.findViewById(R.id.featureGraphic);
            foodNameTextView = (TextView) itemView.findViewById(R.id.foodName);
            calorieTextView = (TextView) itemView.findViewById(R.id.calorieId);
            priceTextView = (TextView) itemView.findViewById(R.id.productPrice);
            addToCartImageView = (ImageView) itemView.findViewById(R.id.addToCartIcon);

            itemView.setOnClickListener(this);
            addToCartImageView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {

            if(v.getId()==addToCartImageView.getId()){
                if(v.getContext() instanceof ClickEventAction){

                    Datum tempFood = foodDataList.get(getAdapterPosition());
                    tempFood.setAddedToCart(!tempFood.isAddedToCart());

                    //Top cart item number update
                    int numberOfItem = tempFood.isAddedToCart() ? preferences.getNumberOfSelectedProduct()+1 : preferences.getNumberOfSelectedProduct()-1;
                    preferences.setNumberOfSelectedProduct(numberOfItem);
                    ((ClickEventAction)v.getContext()).updateNumberOfCart(numberOfItem);

                    //Database and icon update
                    if(tempFood.isAddedToCart()) {
                        foodQueryClass.addToCartTable(tempFood);
                        Picasso.with(v.getContext())
                                .load(R.drawable.ic_favorite_red)
                                .into(addToCartImageView);
                    }
                    else {
                        foodQueryClass.deleteProductById(tempFood.getId());
                        Picasso.with(v.getContext())
                                .load(R.drawable.ic_favorite_border_red)
                                .into(addToCartImageView);
                    }
                }

            }
            else {
                if(v.getContext() instanceof ClickEventAction){
                    ((ClickEventAction)v.getContext()).showFoodDetails(foodDataList.get(getAdapterPosition()));
                }
            }
        }
    }

    public void deleteItem(int position){
        foodDataList.remove(position);
        notifyItemRemoved(position);
    }

    /**
     * Here is the key method to apply the animation
     */
    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(getContext(), android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }


}
