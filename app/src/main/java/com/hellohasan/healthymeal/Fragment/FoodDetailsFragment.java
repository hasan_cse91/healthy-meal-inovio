package com.hellohasan.healthymeal.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.hellohasan.healthymeal.Database.FoodQueryClass;
import com.hellohasan.healthymeal.Model.Datum;
import com.hellohasan.healthymeal.Model.Tag;
import com.hellohasan.healthymeal.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FoodDetailsFragment extends Fragment {

    public ImageView featureGraphics;
    public TextView foodNameTextView;
    public TextView descriptionTextView;
    public TextView calorieTextView;
    public TextView priceTextView;
    public ImageView addToCartImageView;
    public TextView tagsTextView;
    public TextView fatTextView;
    public TextView proteinTextView;

    public FoodDetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_food_details, container, false);
        featureGraphics = (ImageView) view.findViewById(R.id.imageDetails);
        foodNameTextView = (TextView) view.findViewById(R.id.foodNameNameDetails);
        descriptionTextView = (TextView) view.findViewById(R.id.descriptionDetails);
        calorieTextView = (TextView) view.findViewById(R.id.calorieDetails);
        priceTextView = (TextView) view.findViewById(R.id.priceDetails);
//        addToCartImageView = (ImageView) view.findViewById(R.id.addToCartIconDetails);
        tagsTextView = (TextView) view.findViewById(R.id.tagsDetails);
        fatTextView = (TextView) view.findViewById(R.id.fatDetails);
        proteinTextView = (TextView) view.findViewById(R.id.proteinDetails);

        //Receive data
        Bundle bundle = getArguments();
        String foodJson = bundle.getString("data");

        Gson gson = new Gson();
        Datum singleFood = gson.fromJson(foodJson, Datum.class);

        Picasso.with(getContext())
                .load(singleFood.getImages().get(0))
                .into(featureGraphics);
        foodNameTextView.setText(singleFood.getName());
        descriptionTextView.setText("Description:\n" + singleFood.getDescription());
        calorieTextView.setText("Calorie: " + singleFood.getCalorie());
        priceTextView.setText("Price: " + String.valueOf(singleFood.getPrice()) + " BDT");
        fatTextView.setText("Fat: " + singleFood.getFat());
        proteinTextView.setText("Protein: " + singleFood.getProtein());

        List<Tag> tagList = singleFood.getTags();

        for (Tag tag: tagList) {
            tagsTextView.append(tag.getName() + ", ");
        }


        return view;
    }

}
