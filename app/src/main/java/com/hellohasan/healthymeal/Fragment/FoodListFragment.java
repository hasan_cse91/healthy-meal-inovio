package com.hellohasan.healthymeal.Fragment;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.hellohasan.healthymeal.Adapter.FoodListAdapter;
import com.hellohasan.healthymeal.HelperClass.Preferences;
import com.hellohasan.healthymeal.HelperClass.UtilityClass;
import com.hellohasan.healthymeal.Model.Datum;
import com.hellohasan.healthymeal.Model.FoodList;
import com.hellohasan.healthymeal.Model.Tag;
import com.hellohasan.healthymeal.R;
import com.hellohasan.healthymeal.RetrofitNetworkPackage.ApiInterface;
import com.hellohasan.healthymeal.RetrofitNetworkPackage.RetrofitApiClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class FoodListFragment extends Fragment {

    private Preferences preferences;
    private ProgressDialog progressDialog;
    private ApiInterface apiInterface;
    private List<Datum> foodList;
    private List<Tag> tagList;
    private FoodListAdapter foodListAdapter;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;

    public FoodListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_food_list, container, false);

        preferences = Preferences.getInstance(getActivity());

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle("Loading...");
        progressDialog.setMessage("Please wait a few moments");

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);

        recyclerView = (RecyclerView) view.findViewById(R.id.foodRecyclerView);

        apiInterface = RetrofitApiClient.getClient().create(ApiInterface.class);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        if(UtilityClass.isNetworkAvailable(getContext())){
            progressDialog.show();
            loadFoodFromServer();
        }
        else
            Toast.makeText(getContext(), "Turn on Internet connection", Toast.LENGTH_LONG).show();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                if(UtilityClass.isNetworkAvailable(getActivity())){
                    loadFoodFromServer();
                }
                else{
                    Toast.makeText(getActivity(), "Turn on Internet connection", Toast.LENGTH_LONG).show();
                }

            }
        });

        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

    }

    private void loadFoodFromServer() {

        Call<FoodList> call = apiInterface.getFoodList();
        call.enqueue(new Callback<FoodList>() {
            @Override
            public void onResponse(Call<FoodList> call, Response<FoodList> response) {
                if(progressDialog.isShowing())
                    progressDialog.cancel();
                swipeRefreshLayout.setRefreshing(false);
                FoodList foods = response.body();

                if(foods!=null && foods.isSuccess() && foods.getTotal()>0){
                    foodList = foods.getData();
                    showRecyclerView();
                }
                else
                    Toast.makeText(getContext(), "No foods are available", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<FoodList> call, Throwable t) {
                if(progressDialog.isShowing())
                    progressDialog.cancel();
                swipeRefreshLayout.setRefreshing(false);
                Toast.makeText(getContext(), "Failure Message: " + t.toString(), Toast.LENGTH_LONG).show();
            }
        });

    }

    private void showRecyclerView() {
        if(foodList.isEmpty())
            Toast.makeText(getActivity(), "No exam for you!", Toast.LENGTH_LONG).show();
        else {
            foodListAdapter = new FoodListAdapter(getActivity(), foodList);
            recyclerView.setAdapter(foodListAdapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        }
    }

}
