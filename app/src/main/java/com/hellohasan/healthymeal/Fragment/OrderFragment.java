package com.hellohasan.healthymeal.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.hellohasan.healthymeal.Adapter.FoodListAdapter;
import com.hellohasan.healthymeal.Database.FoodQueryClass;
import com.hellohasan.healthymeal.Model.Datum;
import com.hellohasan.healthymeal.R;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrderFragment extends Fragment {

    private FoodQueryClass foodQueryClass;
    private List<Datum> foodList;
    private FoodListAdapter foodAdapter;
    private RecyclerView recyclerView;
    private TextView totalAmountTextView;

    public OrderFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order, container, false);

        totalAmountTextView = (TextView) view.findViewById(R.id.totalAmount);
        foodQueryClass = new FoodQueryClass(getContext());

        foodList = foodQueryClass.getAllFoodFromCart();

        recyclerView = (RecyclerView) view.findViewById(R.id.orderRecyclerView);


        if(foodList!=null) {
            showRecyclerView();
            totalAmountTextView.setText("Total amount:" + calculateCost() + " BDT");
        }
        else
            Toast.makeText(getActivity(), "Something wrong!", Toast.LENGTH_LONG).show();


        return view;
    }

    private String calculateCost() {
        int amount = 0;
        for (Datum food: foodList) {
            amount += food.getPrice();
        }
        return String.valueOf(amount);
    }

    private void showRecyclerView() {
        if(foodList.isEmpty())
            Toast.makeText(getActivity(), "No food in your cart!", Toast.LENGTH_LONG).show();
        else {
            foodAdapter = new FoodListAdapter(getActivity(), foodList);
            recyclerView.setAdapter(foodAdapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        }
    }
}
