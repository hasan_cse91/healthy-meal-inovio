package com.hellohasan.healthymeal.HelperClass;


public class KeyNameClass {


    /**
     * All KEY FOR DATABASE
     */

    // Table names
    public static final String CART_TABLE = "cart_table";

    //Product Table Columns names
    public static final String FOOD_ID = "_id";
    public static final String FOOD_NAME = "name";
    public static final String DESCRIPTION = "description";
    public static final String IMAGE_URL = "image_url";
    public static final String TAGS = "tags";
    public static final String CALORIE = "calorie";
    public static final String FAT = "fat";
    public static final String PROTEIN = "protein";
    public static final String PRICE = "price";

}
