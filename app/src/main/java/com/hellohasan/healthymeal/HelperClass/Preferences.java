package com.hellohasan.healthymeal.HelperClass;

import android.content.Context;
import android.content.SharedPreferences;

public class Preferences {

    private static final String NUMBER_OF_SELECTED_PRODUCT = "cart_product";

    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;

    private static Preferences preferences = new Preferences();

    private Preferences(){}

    public static Preferences getInstance(Context context){
        sharedPreferences = context.getSharedPreferences("sharedPreferences_data", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        return preferences;
    }

    public int getNumberOfSelectedProduct(){
        return sharedPreferences.getInt(NUMBER_OF_SELECTED_PRODUCT, 0);
    }

    public void setNumberOfSelectedProduct(int userId){
        editor.putInt(NUMBER_OF_SELECTED_PRODUCT, userId);
        editor.apply();
        editor.commit();
    }

}
