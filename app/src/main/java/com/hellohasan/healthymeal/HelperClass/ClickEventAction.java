package com.hellohasan.healthymeal.HelperClass;

import com.hellohasan.healthymeal.Model.Datum;

public interface ClickEventAction {

    void updateNumberOfCart(int numberOfItem);

    void showFoodDetails(Datum foodItem);
}
